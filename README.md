# Datasets
This repository contains all of our self-maintained and self-collected data. Find the rest of our curated datasets here: 
[ECCS Datasets Page](https://cluster.earlham.edu/datasets)


## iceland-weather.csv
Collected from the Dalatangi weather station in Iceland.
